import React from 'react';

// Creates UserContext object
/*
	createCOntext methd allows us to store a different data inside an object, in this case called UserContext. This set of information can be shared to other components within the app

	with this method, we will be able to create a global state to store the user details instead of getting it from the localStorage from time to time

	a different approach to passing information between components without the use of props and pass it from parent to child component
*/

const UserContext = React.createContext();

/*
	Provider component allows other components to consume/use the context object and supply the necessary information needed to the context object
*/
// export without default needs destructuring before being able tobe imported by other components

export const UserProvider = UserContext.Provider;

export default UserContext;