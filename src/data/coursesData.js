const coursesData = [
	{
		id: "wdc001",
		name: "PHP-Laravel",
		description: "Curabitur tellus dui, varius id lacus eu, hendrerit hendrerit felis. Mauris nunc dui, eleifend nec quam et, sodales auctor magna. Donec viverra dolor ut velit suscipit maximus. Phasellus faucibus dolor vitae felis fringilla mattis. Donec vestibulum fermentum tellus, ac mattis dolor bibendum ac. Proin a massa et tortor sagittis suscipit. Integer ante leo, condimentum nec laoreet in, tempus posuere felis. Sed at ante nec quam posuere elementum nec ac tortor.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python-Django",
		description: "Curabitur tellus dui, varius id lacus eu, hendrerit hendrerit felis. Mauris nunc dui, eleifend nec quam et, sodales auctor magna. Donec viverra dolor ut velit suscipit maximus. Phasellus faucibus dolor vitae felis fringilla mattis. Donec vestibulum fermentum tellus, ac mattis dolor bibendum ac. Proin a massa et tortor sagittis suscipit. Integer ante leo, condimentum nec laoreet in, tempus posuere felis. Sed at ante nec quam posuere elementum nec ac tortor.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java-Springboot",
		description: "Curabitur tellus dui, varius id lacus eu, hendrerit hendrerit felis. Mauris nunc dui, eleifend nec quam et, sodales auctor magna. Donec viverra dolor ut velit suscipit maximus. Phasellus faucibus dolor vitae felis fringilla mattis. Donec vestibulum fermentum tellus, ac mattis dolor bibendum ac. Proin a massa et tortor sagittis suscipit. Integer ante leo, condimentum nec laoreet in, tempus posuere felis. Sed at ante nec quam posuere elementum nec ac tortor.",
		price: 45000,
		onOffer: true
	}
]

export default coursesData;