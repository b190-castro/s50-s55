import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext';



export default function Logout() {
	const { unsetUser, setUser } = useContext(UserContext);

	// clear the localStorage
	unsetUser();

	/*
		placing the setUser setter function inside of a useEffect is necessary because of the updates in React JS that a state of another component cannot be updated while trying to render a different component

		by adding useEffect, this will render Logout page first before triggering the useEffect which changes the state of the user
	*/

	useEffect(() => {
		// Set the user state back to it's original state
		setUser({id:null});
	})

	return(
		<Navigate to='/login' />
	)
}