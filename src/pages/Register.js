import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react'
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2'

export default function Register(){
		/*
			Miniactivity
				1. create email, password1 and password2 variables using useState and set their initial value to empty string ""
				2. create is Active Variable using useState and set the initial value to false
				3. refactor the button and add a ternary operator stating that if the isActive is true, the button will be clickable, otherwise, disabled
		*/

			const navigate = useNavigate();
			const { user } = useContext(UserContext);
			const [ firstName, setFirstName ] = useState('');
			const [ lastName, setLastName ] = useState('');
			const [ email, setEmail ] = useState('');
			const [ password1, setPassword1 ] = useState('');
			const [ password2, setPassword2 ] = useState('');
			const [ mobileNumber, setMobileNumber ] = useState('');
			const [ isActive, setIsActive ] = useState(false);

			// to check if we have successfully implemented the 2-way binding
			/*
				whenever a state of a component changes, the component renders the whole component executing the code found in the component itself.
				e.target.value allows us to gain access the input field's current value to be used when submitting form data
			*/

			console.log(email);
			console.log(firstName);
			console.log(lastName);
			console.log(mobileNumber);
			console.log(password1);
			console.log(password2);

			useEffect(()=>{
				if( (email !==''&& password1 !=='' && password2 !=='') && (password1 === password2)
					){
					setIsActive(true);
				}else{
					setIsActive(false);
				}
			},[email,password1,password2]);

			useEffect(()=>{
				if(!isNaN(mobileNumber) && mobileNumber.length == 11){
					setIsActive(true);
				}else{
					setIsActive(false);
				}
			})

			function registerUser(e){
				e.preventDefault();

				fetch('http://localhost:4000/users/checkEmail',{
					method: 'POST',
					headers:{
						'Content-type': 'application/json'
					},
					body: JSON.stringify({
						email: email,
				})
				})
				.then(res => res.json())
				.then(data => {
					if (data == true){
						Swal.fire({
							title:"Duplicate mail found",
							icon:"error",
							text:"Please provide a different email"
						})
					} else {
						Swal.fire({
							title:"Registration Successful",
							icon:"success",
							text:"Welcome to Zuitt"
						})
						navigate('/login');
					}
				})

				

				// clear input fields
				setEmail('');
				setFirstName('');
				setLastName('');
				setMobileNumber('');
				setPassword1('');
				setPassword2('');

				// alert('Thank you for registering!');
			};

		return (
			(user.email !== null) ?
			<Navigate to='/courses' />
			:
	    <Form onSubmit={(e) => registerUser(e)}>
	      <Form.Group controlId="firstName">
	      	<Form.Label>First name</Form.Label>
	      	<Form.Control
	      		type="firstName"
	      		placeholder="Enter first name"
	      		value={firstName}
	      		onChange={e => setFirstName(e.target.value)}
	      		required
	      	/>
	      </Form.Group>

	      <Form.Group controlId="lastName">
	      	<Form.Label>Last name</Form.Label>
	      	<Form.Control 
	      		type="lastName"
	      		placeholder="Enter last name"
	      		value={lastName}
	      		onChange={e => setLastName(e.target.value)}
	      		required
	      	/>
	      </Form.Group>

	      <Form.Group controlId="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control 
	        	type="email" 
	        	placeholder="Enter email" 
	        	value={email}
	        	onChange={e => setEmail(e.target.value)}
	        	required
	        />
	        <Form.Text className="text-muted">
	          We'll never share your email with anyone else.
	        </Form.Text>
	      </Form.Group>

	      <Form.Group controlId="mobileNumber">
	      	<Form.Label>Mobile number</Form.Label>
	      	<Form.Control 
	      		type="mobileNumber"
	      		placeholder="Enter mobile number"
	      		value={mobileNumber}
	      		onChange={e => setMobileNumber(e.target.value)}
	      		required
	      	/>
	      </Form.Group>

	      <Form.Group controlId="password1">
	        <Form.Label>Password</Form.Label>
	        <Form.Control 
	        	type="password" 
	        	placeholder="Password"
	        	value={password1}
	        	onChange={e => setPassword1(e.target.value)}
	        	required
	         />
	      </Form.Group>

	      <Form.Group controlId="password2">
	        <Form.Label>Verify Password</Form.Label>
	        <Form.Control 
	        	type="password" 
	        	placeholder="Password"
	        	value={password2}
	        	onChange={e => setPassword2(e.target.value)}
	        	required
	         />
	      </Form.Group>
	      {isActive ?
	      <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
	      :
	      <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
	  	  }
	    </Form>
	  );
}